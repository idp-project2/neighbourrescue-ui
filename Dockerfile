FROM node:16

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY package*.json ./

RUN npm install --silent --production
RUN npm install react-scripts@5.0.1 -g --silent

COPY . .

EXPOSE 3000
CMD ["npm", "start"]
