import './Menu.css'
import Button from '@mui/material/Button';
import { Box, color } from '@mui/system';
import {useAuth0} from '@auth0/auth0-react';
import React, { Component } from 'react'
import { Navbar, NavDropdown, Form, FormControl, Nav } from 'react-bootstrap'
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
} from "react-router-dom";

import FindTransfer from "./components/FindTransfer"
import AvailableTransfers from "./components/AvailableTransfers"
import PostTransfer from "./components/PostTransfer"
import AcceptedTransfers from "./components/AcceptedTransfers"
import Register from './Register';
import axios from 'axios'
import {useState} from 'react';

export default function Menu(props) {
    const {loginWithRedirect, isAuthenticated, logout, user} = useAuth0();
  if(!isAuthenticated) {
    return ( 
        <div className='menu'>
                <Button sx= {{
                    // backgroundColor: "#efacf5",
                    height: "30px",
                    fontSize: "20px",
                    width: "400px",
                    boxShadow:3,
                    color:"rgb(255, 165, 124)"
                }} onClick = {()=> loginWithRedirect()}>
                    Login
                </Button>
                <Button sx= {{
                    // backgroundColor: "#efacf5",
                    height: "30px",
                    fontSize: "20px",
                    width: "400px",
                    boxShadow:3,
                   marginTop: "100px",
                    color:"rgb(255, 165, 124)"
                }} onClick={() => props.setPage(3)}>
                    About
                </Button>
        </div>)
    }
    return (
        <Router>
            <div  className='menu'>
          
                <Navbar variant="light" expand="lg" fixed = "top">
                    <Navbar.Text href="#">{props.username}</Navbar.Text>
                    <Navbar.Toggle aria-controls="navbarScroll" />
                    <Navbar.Collapse id="navbarScroll">
                        <Nav
                            className="mr-auto my-2 my-lg-0"
                            style={{ maxHeight: '100px' }}
                            navbarScroll
                        >
                            <Nav.Link as={Link} to="/find">Active trip</Nav.Link>
                            <Nav.Link as={Link} to="/available-transfers">Available trips</Nav.Link>
                            <Nav.Link as={Link} to="/accepted-transfers">Pending Trips</Nav.Link>
                            <Nav.Link as={Link} to="/post-transfer">Post a trip</Nav.Link>
                            <Nav.Link as={Link} to="/register">Submit Profile</Nav.Link>
                            <Button  bg = "info" variant = {"info"} expand = "lg" onClick = {() => logout()}>Logout</Button>
                        </Nav>
                    </Navbar.Collapse>                   
                </Navbar>
                
            </div>
            <div  className='menu'>
                <Routes>
                    <Route path="/find" element={<FindTransfer />} />
                    <Route path="/available-transfers" element={<AvailableTransfers />} />
                    <Route path="/accepted-transfers" element={<AcceptedTransfers />} />
                    <Route path="/post-transfer" element={<PostTransfer />} />
                    <Route path="/register" element={<Register/>} />
                </Routes>
            </div>
        </Router>
    )
}