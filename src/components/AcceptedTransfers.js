import {Button, Box} from '@mui/material';
import { useState, useEffect } from 'react';
import {useAuth0} from '@auth0/auth0-react';
import { useNavigate } from "react-router-dom";

async function getUser(username) {
    var response = await fetch('http://127.0.0.1/api/manage/users/user/' + username)
    .then(data => data.json())

    return response;
}

async function getForms() {
    var response = await fetch('http://127.0.0.1/api/manage/forms/')
    .then(data => data.json())

    return response;
}

export default function AcceptedTransfers() {
    const {user, isAuthentificated} = useAuth0();
    const [currUser, setCurrUser] = useState({});
    const [currFrom, setCurrForm] = useState({});
    const [list, setList] = useState([]);
    const navigate = useNavigate();

    const acceptDriver = (driver, formId) => {
      var response = fetch('http://127.0.0.1/api/manage/forms/acceptFormPassenger/' + driver, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({formId: formId})
      }).then(data => data.json())

      navigate('/');

      return response;
    }
    
    useEffect(() => {
        let mounted = true;
        getUser(user.name)
          .then(u => {
            if(mounted) {
              setCurrUser(u);
            }
          });

        getForms()
          .then(items => {
            if(mounted) {
              setCurrForm(items.filter(e => e.passenger === user.name)[0]);
              setList(items);
            }
          });
        return () => mounted = false;
      }, [])

    return (<div style = {{
        top: "10%",
        position: "absolute",
        display: "flex",
        flexDirection: "column",
        textAlign: "center"
    }}>
        <h1 style = {{
            color: "rgb(248, 87, 87)"
        }}>Pending Trips</h1>
        {!currUser ? <p> Loading... </p> : !currUser[0] ? <p> Loading... </p> : currUser[0].driver ? 
            <div>
              <ul style={{listStyle:"none"}}>
              {
                  list.filter(e => e.drivers.includes(currUser[0].username)).map(elem => <li key= {elem._id}>
                      <Box sx= {{
                          borderRadius: "20px",
                          backgroundColor: "rgb(250, 223, 74)",
                          height: "120px",
                          fontSize: "15px",
                          width: "300px",
                          marginTop: "20px",
                          textAlign: "center",
                      }}>
                          <p style = {{color: "rgb(248, 87, 87)"}}>
                              Start Point: {elem.startPoint}
                          </p>
                          <p style = {{color: "rgb(248, 87, 87)"}}>
                              Finish Point: {elem.finishPoint}
                          </p>
                          <p style = {{color: "rgb(248, 87, 87)"}}>
                              Passanger username: {elem.passenger}
                          </p>
                      </Box>
                  </li>)
              }
              </ul>
            </div> : 
            currFrom ? <div> 
            <Button sx= {{
                        borderRadius: "20px",
                        backgroundColor: "rgb(250, 223, 74)",
                        height: "120px",
                        fontSize: "10px",
                        width: "300px",
                        marginTop: "20px",
                        textAlign: "center",
                    }}>
                        
                        <p style = {{color: "rgb(248, 87, 87)"}}>
                            Start Point: {currFrom.startPoint}
                        </p>
                        <p style = {{color: "rgb(248, 87, 87)"}}>
                            Finish Point: {currFrom.finishPoint}
                        </p>
                        <p style = {{color: "rgb(248, 87, 87)"}}>
                            Passanger username: {currFrom.passenger}
                        </p>
                    </Button>

                    <h2 style = {{
                        color: "rgb(248, 87, 87)"
                    }}>Drivers: </h2>

                    {currFrom.drivers ?
                        currFrom.drivers.map(driver => <li key= {driver}>
                            <Button sx= {{
                                borderRadius: "20px",
                                backgroundColor: "rgb(250, 223, 74)",
                                height: "50px",
                                fontSize: "10px",
                                width: "300px",
                                marginTop: "20px",
                                textAlign: "center",
                            }} onClick={() => acceptDriver(driver, currFrom._id)}>
                                <p style = {{color: "rgb(248, 87, 87)"}}>
                                    {driver}
                                </p>
                            </Button>
                        </li>)
                    : <p> Loading... </p>}

            </div> : <div> No pending trips.. </div>}
    </div>);
}