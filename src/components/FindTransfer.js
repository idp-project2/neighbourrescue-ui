import React from 'react'
import { useEffect, useRef, useState } from "react";
import axios from "axios";
import { createPortal } from "react-dom";
import { Form, Button, Card, Alert } from "react-bootstrap";
import {useAuth0} from '@auth0/auth0-react';


const FindTransfer = () => {
    const startPointRef = useRef()
    const finishPointRef = useRef()
    return (
    <Card  style={{flex:1, backgroundColor:'rgb(250, 223, 74)'}}>  
         <Card.Body>
            <h2 className = "text-center mb-4"> Where do you want to go?</h2>
                <Form>
                    <Form.Group id="starting point" >
                        <Form.Label>Starting Point</Form.Label>
                        <Form.Control type="text" ref={startPointRef} required  style={{ backgroundColor:'rgb(255, 219, 208)'}}></Form.Control>
                    </Form.Group>
                    <Form.Group id="destination">
                        <Form.Label>Destination</Form.Label>
                        <Form.Control type="text" ref={finishPointRef} required style={{ backgroundColor:'rgb(255, 219, 208)'}}></Form.Control>
                    </Form.Group>
                    <Button className ="btn btn-primary" type="submit" bg="light" variant={"dark"} expand="lg">
                                Submit
                    </Button>
                </Form>    
        </Card.Body>
    </Card>
  )
}

export default FindTransfer 
                