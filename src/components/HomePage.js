import React, { useState } from "react";
import {useAuth0} from '@auth0/auth0-react';
import { Navbar, Container, NavDropdown, Button } from "react-bootstrap";

const HomePage = () => {
    const {user, logout, isAuthenticated} = useAuth0();

    //if(isAuthenticated){
        return (
          <Container>
              <Navbar href = '/'>
                    <Navbar.Text>{user.name}</Navbar.Text>
                    <NavDropdown href = '/'>
                        Manage transfers
                        <NavDropdown.Item href = '/request'>Request transfers</NavDropdown.Item>
                        <NavDropdown.Item href = '/accepted'>Accepted transfers</NavDropdown.Item>
                    </NavDropdown>
                    <Button onClick = {logout()}> Log Out </Button>
              </Navbar>
          </Container>  
        )
    //}
}

export default HomePage