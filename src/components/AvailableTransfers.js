import Button from '@mui/material/Button';
import { useState, useEffect } from 'react';
import {useAuth0} from '@auth0/auth0-react';
import { useNavigate } from "react-router-dom";

async function getForms() {
    var response = await fetch('http://127.0.0.1/api/manage/forms/')
    .then(data => data.json())

    return response;
}

export default function AvailableTransfers() {
    const [list, setList] = useState([]);
    const navigate = useNavigate();
    const { user} = useAuth0();
    
    useEffect(() => {
        let mounted = true;
        getForms()
          .then(items => {
            if(mounted) {
              setList(items)
            }
          })
        return () => mounted = false;
      }, [])
    return (<div style = {{
        display: "flex",
        justifyContent:"center",
        position: "absolute",
        display: "flex",
        flexDirection: "column",
        textAlign: "center"
    }}>
        <h1 style = {{
            color: "rgb(248, 87, 87)"
        }}>Available Trips</h1>
        <ul style={{listStyle:"none"}}>

        {
            list.map(elem => <li key= {elem.passenger}>
                <Button sx= {{
                    borderRadius: "20px",
                    backgroundColor: "rgb(250, 223, 74)",
                    height: "120px",
                    fontSize: "10px",
                    width: "300px",
                    marginTop: "20px",
                    textAlign: "center",
                }}onClick={() => {
                        const data = {"formId": elem._id}
                        const requestOptions = {
                            method: 'POST',
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(data)};
                        var route = 'http://127.0.0.1/api/manage/forms/acceptFormDriver/'+ user.name; 
                        const response = fetch(route, requestOptions);
                        navigate('/');
                        }}
                >
                    <p style = {{
            color: "rgb(248, 87, 87)"
        }}>
                        Start Point: {elem.startPoint}
                    </p>
                    <p style = {{color: "rgb(248, 87, 87)"}}>
                        Finish Point: {elem.finishPoint}
                    </p>
                    <p style = {{color: "rgb(248, 87, 87)"}}>
                        Passanger username: {elem.passenger}
                    </p>
                </Button>
            </li>)
        }
        </ul>
    </div>);
}