import React from 'react'
import { useEffect, useRef, useState } from "react";
import axios from "axios";
import { createPortal } from "react-dom";
import { Form, Button, Card, Alert } from "react-bootstrap";
import {useAuth0} from '@auth0/auth0-react';

const PostTransfer = () => {
    const startPointRef = useRef()
    const finishPointRef = useRef()
    const numberOfPassengersRef = useRef()
    const [error, setError] = useState('')
    const {user, isAuthentificated} = useAuth0();
    
    const postTransfer = async (e) =>{
        e.preventDefault();
        var credentials = {
            startPoint: startPointRef.current.value,
            finishPoint: finishPointRef.current.value,
            numberPeople: numberOfPassengersRef.current.value,
            passenger: user.name
        }
        try {
            const response = await axios.post('http://127.0.0.1/api/manage/forms/addForm', 
            credentials,
            {
                startPoint: startPointRef,
                finishPoint: finishPointRef,
                passenger: user.name,
                numberPeople: numberOfPassengersRef
                
            }).then((response)=>{
                alert(response.data);
                 console.log(response)
                 window.location='/postTransfer';
            })
        } catch (err) {
            if (!err.response) {
               setError('No response from server')
            } else if (err.response.status === 200) {
                setError('Missing username or password')
            } else {
                setError("Invalid credentials")
            }
        }
    
    }
    return (
        <div className=''>
                <Card  style={{flex:1, backgroundColor:'rgb(250, 223, 74)'}}>  
                    <Card.Body>
                        <h2 className = "text-center mb-4"> Where do you want to go?</h2>
                        <Form onSubmit={postTransfer}>
                            <Form.Group id="starting point">
                                <Form.Label>Starting Point</Form.Label>
                                <Form.Control type="text" ref={startPointRef} required style={{ backgroundColor:'rgb(255, 219, 208)'}}></Form.Control>
                            </Form.Group>
                            <Form.Group id="destination">
                                <Form.Label>Destination</Form.Label>
                                <Form.Control type="text" ref={finishPointRef} required style={{ backgroundColor:'rgb(255, 219, 208)'}}></Form.Control>
                            </Form.Group>
                            <Form.Group id="number of passengers">
                                <Form.Label>Number of passengers</Form.Label>
                                <Form.Control type="text" ref={numberOfPassengersRef} required style={{ backgroundColor:'rgb(255, 219, 208)'}}></Form.Control>
                            </Form.Group>
                            <Button className ="btn btn-primary" type="submit" bg="dark" variant={"dark"} expand="lg">
                                Submit
                            </Button>
                        </Form>
                    </Card.Body>
                </Card>
            </div>
    )
}

export default PostTransfer