import React, { Component } from 'react'
import { Navbar, NavDropdown, Form, FormControl, Button, Nav } from 'react-bootstrap'
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link
} from "react-router-dom";
import {useAuth0} from '@auth0/auth0-react';
import FindTransfer from "./FindTransfer"
import AvailableTransfers from "./AvailableTransfers"
import PostTransfer from "./PostTransfer"
import AcceptedTransfers from "./AcceptedTransfers"
import axios from 'axios'
import {useState} from 'react';

const NavbarComp = () => {
    const [error, setError] = useState('')
    const {user, logout, isAuthenticated} = useAuth0();
    const addUser = async (e) =>{
        var credentials = {
            username:e.name,
            name: e.sub
        }
        try {
            const response = await axios.post('http://127.0.0.1/api/manage/users/register', 
            credentials,
            {
                username: user.name,
                name: user.sub
    
                
            }).then((response)=>{
                alert(response.data);
                 console.log(response)
                 window.location='/dashboard';
            })
        } catch (err) {
            if (!err.response) {
               setError('No response from server')
            } else if (err.response.status === 200) {
                setError('Missing username or password')
            } else {
                setError("Invalid credentials")
            }
        }
    
    }
    addUser(user);
 //   if(isAuthenticated){
        return (
            <Router>
                <div>
                    <Navbar bg="dark" variant={"dark"} expand="lg">
                        <Navbar.Text href="#">{user.name}</Navbar.Text>
                        <Navbar.Toggle aria-controls="navbarScroll" />
                        <Navbar.Collapse id="navbarScroll">
                            <Nav
                                className="mr-auto my-2 my-lg-0"
                                style={{ maxHeight: '100px' }}
                                navbarScroll
                            >
                                <Nav.Link as={Link} to="/find">Find a transfer</Nav.Link>
                                <Nav.Link as={Link} to="/available-transfers">Available transfers</Nav.Link>
                                <Nav.Link as={Link} to="/accepted-transfers">Accepted transfers</Nav.Link>
                                <Nav.Link as={Link} to="/post-transfer">Post a transfer</Nav.Link>

                            </Nav>

                        </Navbar.Collapse>
                        <Button bg = "info" variant = {"info"} expand = "lg" onClick = {() => logout()}>Logout</Button>
                    </Navbar>
                </div>
                <div>
                    <Routes>
                        <Route path="/find" element={<FindTransfer />} />
                        <Route path="/available-transfers" element={<AvailableTransfers />} />
                        <Route path="/accepted-transfers" element={<AcceptedTransfers />} />
                        <Route path="/post-transfer" element={<PostTransfer />} />
                    </Routes>
                </div>
            </Router>
        )
//    }
}
export default NavbarComp