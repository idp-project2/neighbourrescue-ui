import {useAuth0} from '@auth0/auth0-react';
import { Button, Card } from 'react-bootstrap'


import React from 'react'

const StartingPage = () => {

    const {loginWithRedirect, isAuthenticated} = useAuth0();

    return (
        !isAuthenticated && (
            <div className = "h-50 w-50">
                <Card className = "d-flex">
                    <Card.Body>
                        <Card.Title>Ceva nush inca</Card.Title>
                            <Card.Text>Blablabla
                            </Card.Text>
                                <Button className = "align-items-center" bg = "info" variant = {"info"} expand = "lg"
                                        onClick = {()=> loginWithRedirect()}>
                                    GetStarted
                                </Button>
                            
                    </Card.Body>
                </Card>
            </div>
        )
        
    )
}

export default StartingPage