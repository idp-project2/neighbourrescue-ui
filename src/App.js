import { useState } from 'react';
import './App.css';
import picture from './Pictures/road_pic.jpg'
 import SignIn from './SignIn';
 import Register from './Register';
 import Menu from './Menu'
 import About from './AboutUs'
 import MenuUser from './MenuUser'
 import NavbarComp from './components/NavbarComp';
 import PostTransfer from './components/PostTransfer';
 import HomePage from './components/HomePage';
 import StartingPage from './components/StartingPage';

function App(props) {
  const [page, setPage] = useState(0);
  const [username, setUsername] = useState("");
  const handleClick = () => {
    setPage(1);
  };
  return (
    <div className="App">
      <div class="circle" style ={{ top: "-100px", right: "-100px" }}>
      </div>
      <div class="circle" style ={{ top: "550px", right:"1300px" }}>   
        </div>

       {
        page === 0 ? <Menu setPage = {setPage}/> : 
        page === 1 ? <Register setPage = {setPage}/>:
        page === 2 ? <SignIn setPage = {setPage}/>:
        page === 3 ? <About setPage = {setPage}/>:
        page === 4 ? <NavbarComp setPage = {setPage}/>:
        page === 5 ? <PostTransfer setPage = {setPage}/>:

        <div>
          No page available
        </div>
     } 
    </div>
  );
}

export default App;

 