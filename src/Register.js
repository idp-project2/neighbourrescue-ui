import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useNavigate } from "react-router-dom";
import {useAuth0} from '@auth0/auth0-react';



const theme = createTheme();

export default function Register(props) {
  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);

    console.log(data);
    var username = data.get('Username');
    var email = data.get('email');
    var name = data.get('name');
    var surname = data.get('surname');
    const newData = {   "username": user.name,
                        "email": user.name,
                        "name": name,
                        "surName": surname,
                        "driver": data.get("role")};
    console.log(newData);
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(newData)};
    var route = 'http://127.0.0.1/api/manage/users/register'; 
    const response = fetch(route, requestOptions);
    navigate('/');
    return response;
  };
  const navigate = useNavigate();
  const {isAuthenticated, user} = useAuth0();
  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 2,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            marginTop: "-20px"
          }}
        >
          <Typography component="h1" variant="h5" style={{color: "rgb(255, 165, 124)"}}>
            Hello, new User
          </Typography>
            <Box component="form" style={{color: "rgb(255, 165, 124)"}} onSubmit={handleSubmit} noValidate sx={{ color: "rgb(255, 165, 124)"}}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="surname"
              label="surname"
              name= "surname"
              autoFocus/>
                        <TextField
              margin="normal"
              required
              fullWidth
              id="name"
              label="name"
              name = "name"
              autoFocus
            />
                    <div>
        <input type="radio" value="true" name="role" /> Driver
        <input type="radio" value="false" name="role" /> Passenger
        </div>
             <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ backgroundColor:"rgb(255, 165, 124)"}}
            >
              Submit Profile
            </Button>
            </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}