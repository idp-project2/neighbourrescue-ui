import { Text, StyleSheet } from "react-native";
import React, { useState } from "react";
import Button from '@mui/material/Button';




export default function About(props) {

    const [titleText, setTitleText] = useState("About Give me a Ride");
    const bodyText1 = "Firstly, you need to login or register via the login button from the first page. After all that is done, you will be redirected to the main menu of the app.";
    const bodyText2 = "Now, the first thing you need to do is to complete your profile at the submit profile button. After this you are good to go!";
    const bodyText3 = "You can choose your role as a driver or a passenger, a driver can accept trips that a passenger posted.";
  
    const onPressTitle = () => {
      setTitleText("How it works?");
    };
  
    return (
      <Text style={styles.baseText}>
        <Text style={styles.titleText} onPress={onPressTitle}>
          {titleText}
          {"\n"}
          {"\n"}
        </Text>
        <Text numberOfLines={5}>{bodyText1}</Text>
        <Text numberOfLines={5}>{bodyText2}</Text>
        <Text numberOfLines={5}>{bodyText3}</Text>
        <Button sx= {{
                    // backgroundColor: "#efacf5",
                    height: "30px",
                    fontSize: "20px",
                    width: "200px",
                    boxShadow:3,
                   marginTop: "100px",
                    color:"rgb(98, 87, 253)"
                }} onClick={() => props.setPage(0)}>
                    Go Back
                </Button>
      </Text>
    );
  };
  
  const styles = StyleSheet.create({
    baseText: {
      fontFamily: "Cochin",
      color: "rgb(98, 87, 253)",
      fontSize: 20
    },
    titleText: {
        fontSize: 30,
        fontWeight: "bold"
      }
    });